<?php
	require_once dirname("../").'/lib/ActiveResource.php';

	class Project extends ActiveResource {
		var $site = 'jean-noel.aubry:*Aubry34@srvrd:3000/redmine/';
		var $request_format = 'json';
		var $element_name = 'project';
	}
	
	// Load list of projects
	$i=0;
	$projectsList = [];
	$project = new Project();
	$params['limit'] = '1000';
	$projects = $project->find('all', $params);
	foreach ($projects as $value) {
		// Only load id and name of project
		$projectsList[$i]['id'] = $value->id;
		$projectsList[$i]['name'] = $value->name;
		$i++;
	}
	
	// get default data for dashboard
	class Issue extends ActiveResource {
		var $site = 'jean-noel.aubry:*Aubry34@srvrd:3000/redmine/';
		var $request_format = 'json';
		var $element_name = 'issue';
	}
	
	// get last 24 hours date
	$last24hrs = date("Y-m-d", strtotime("-1 day"));
	
	// get last week date
	$lastWeek = date("Y-m-d", strtotime("-1 week"));
	
	//========================================== ROW 1 ==========================================
	// Row 1 - Widget 1 => Total number of issues updated (last 24 hrs)
	$issue_updated = new Issue();
	$params['updated_on'] = '>='.$last24hrs;
	$params['status_id'] = '*';
	$params['limit'] = '1000';
	if (!empty($_POST['projects-select']))
		$params['project_id'] = $_POST['projects-select'];
	$issues_updated = sizeof($issue_updated->find('all', $params));
	
	// Row 1 - Widget 2 => Total number of new issues (last 24 hrs)
	$issue_new = new Issue();
	$params['status_id'] = '1';
	$issues_new = sizeof($issue_new->find('all', $params));
	
	// Row 1 - Widget 3 => Total number of closed issues (last 24 Hrs)
	$issue_closed = new Issue();
	$params['status_id'] = 'closed';
	$issues_closed = sizeof($issue_closed->find('all', $params));
	
	// Row 1 - Widget 4 => Total new customer issues (Filter on custom field 'cf_8' => clients filter)
	$new_customer_issue = new Issue();
	$params['cf_8'] = '*';
	$params['status_id'] = '1';
	$params['updated_on'] = '>='.$lastWeek;
	$new_customer_issues = sizeof($new_customer_issue->find('all', $params));
	
	//========================================== ROW 2 ==========================================
	// Row 2 - Widget 1 - Trends => All customer issues opened in the last 3 months
	
	// Row 2 - Widget 2 - Trends => All issues opened in the last 3 months
	
	// Row 2 - Widget 3 - Trends => All issues closed in the last 3 months
	
?>