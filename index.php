<?php
	require_once ('controllers/loader.php');
?>
<!DOCTYPE html>
<html lang="fr" class="perfect-scrollbar-on">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<title>Redmine Reports</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport">
	<meta name="description" content="Redmine">
	<meta name="keywords" content="issue,bug,tracker">
	<link rel="shortcut icon" href="favicon.ico">
	
	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	
	<!-- CSS Files -->
	<link href="assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet">
	
	</head>
	<body class>
		<div class="wrapper">
			<div class="sidebar" data-color="azure" data-background-color="white">
				<!--
					Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

					Tip 2: you can also add an image using data-image tag
				-->
				<div class="logo">
					<a href="http://localhost" class="simple-text logo-normal">
						<img src="assets/img/IES.png" style="width:30%"> Redmine Reports
					</a>
				</div>
				<div class="sidebar-wrapper ps-container ps-theme-default" data-ps-id="eb5c4d77-3fad-5aaa-c6ed-bce8fc64eeeb">
					<ul class="nav">
					  <li class="nav-item active  ">
						<a class="nav-link" href="./dashboard.html">
						  <i class="material-icons">dashboard</i>
						  <p>Dashboard</p>
						</a>
					  </li>
					  <li class="nav-item ">
						<a class="nav-link" href="./user.html">
						  <i class="material-icons">bug_report</i>
						  <p>Issues</p>
						</a>
					  </li>
					  <li class="nav-item ">
						<a class="nav-link" href="./tables.html">
						  <i class="material-icons">list</i>
						  <p>Test Cases</p>
						</a>
					  </li>
					  <li class="nav-item ">
						<a class="nav-link" href="./server.html">
						  <i class="material-icons">memory</i>
						  <p>Server status</p>
						</a>
					  </li>
					  <li class="nav-item ">
						<a class="nav-link" href="./typography.html">
						  <i class="material-icons">settings</i>
						  <p>Settings</p>
						</a>
					  </li>
					</ul>
					<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
						<div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
					</div>
					<div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;">
						<div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
					</div>
				</div>
			</div>
			<div class="main-panel ps-container ps-theme-default">
				 <!-- Navbar -->
				<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
					<div class="container-fluid">
						<div class="navbar-wrapper">
							<a class="navbar-brand" href="#pablo">Dashboard</a>
						</div>
						<div class="collapse navbar-collapse justify-content-end">
							<form class="navbar-form">
								<span class="bmd-form-group">
									<div class="input-group no-border">
										<input type="text" value="" class="form-control" placeholder="Search...">
										<button type="submit" class="btn btn-white btn-round btn-just-icon">
											<i class="material-icons">search</i>
											<div class="ripple-container"></div>
										</button>
									</div>
								</span>
							</form>
						</div>
						<div class="navbar-wrapper justify-content-end">
							&nbsp;&nbsp;Project:&nbsp;&nbsp;
							<form action="" method="post" id="projects-list">
								<select name="projects-select" id="projects-select" class="form-control">
									<?php
										echo '<option value="0" selected>All</option>';
										if (isset($_POST["projects-select"])) 
											$projectId = $_POST["projects-select"];
										else
											$projectId = "0";
				
										for ($i=0; $i< sizeof($projectsList); $i++) {
											echo '<option ';
											if ($projectId == $projectsList[$i]['id']) 
												echo 'selected';
											echo ' value="'.$projectsList[$i]['id'].'">'.$projectsList[$i]['name'].'</option>';
										}
									?>
								</select>
							</form>
						</div>
					</div>
				</nav>
				<!-- End Navbar -->
				<!-- Content -->
				<div class="content">
					<div class="container-fluid">
					<!-- 1st row -->
						<div class="row">
							<div class="col-lg-3 col-md-6 col-sm-6">
							  <div class="card card-stats">
								<div class="card-header card-header-warning card-header-icon">
								  <div class="card-icon">
									<i class="material-icons">score</i>
								  </div>
								  <p class="card-category">Total Issues updated</p>
								  <h3 class="card-title" id="issues_updated"><?php echo $issues_updated; ?>
								  </h3>
								</div>
								<div class="card-footer">
								  <div class="stats">
									<i class="material-icons">date_range</i> Last 24 Hours
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-lg-3 col-md-6 col-sm-6">
							  <div class="card card-stats">
								<div class="card-header card-header-danger card-header-icon">
								  <div class="card-icon">
									<i class="material-icons">info_outline</i>
								  </div>
								  <p class="card-category">Total number of new Issues</p>
								  <h3 class="card-title"id="issues_new"><?php echo $issues_new; ?></h3>
								</div>
								<div class="card-footer">
								  <div class="stats">
									<i class="material-icons">date_range</i> Last 24 Hours
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-lg-3 col-md-6 col-sm-6">
							  <div class="card card-stats">
								<div class="card-header card-header-success card-header-icon">
								  <div class="card-icon">
									<i class="fa fa-check-square"></i>
								  </div>
								  <p class="card-category">Total number of closed Issues</p>
								  <h3 class="card-title"><?php echo $issues_closed; ?></h3>
								</div>
								<div class="card-footer">
								  <div class="stats">
									<i class="material-icons">date_range</i> Last 24 Hours
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-lg-3 col-md-6 col-sm-6">
							  <div class="card card-stats">
								<div class="card-header card-header-info card-header-icon">
								  <div class="card-icon">
									<i class="material-icons">bug_report</i>
								  </div>
								  <p class="card-category">Total new customer Issues (Last week)</p>
								  <h3 class="card-title"><?php echo $new_customer_issues; ?></h3>
								</div>
								<div class="card-footer">
								  <div class="stats">
									<i class="material-icons text-danger">warning</i> To investigate ASAP
								  </div>
								</div>
							  </div>
							</div>
						</div>
						<!-- 2nd row -->
						<div class="row">
							<div class="col-md-4">
								<div class="card card-chart">
									<div class="card-header card-header-success">
										<!-- Widget 1 - Trends => All customer issues opened in the last 3 months -->
										<div class="ct-chart" id="OpenCustomerIssuesChart"></div>
									</div>
									<div class="card-body">
									  <h4 class="card-title">Daily Sales</h4>
									  <p class="card-category">
										<span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
									</div>
									<div class="card-footer">
									  <div class="stats">
										<i class="material-icons">access_time</i> updated 4 minutes ago
									  </div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="card card-chart">
									<div class="card-header card-header-warning">
									  <div class="ct-chart" id="websiteViewsChart"><svg width="100%" height="30%" class="ct-chart-bar" style="width: 100%; height: 30%;"><g class="ct-grids"><line y1="120" y2="120" x1="40" x2="448.328125" class="ct-grid ct-vertical"></line><line y1="96" y2="96" x1="40" x2="448.328125" class="ct-grid ct-vertical"></line><line y1="72" y2="72" x1="40" x2="448.328125" class="ct-grid ct-vertical"></line><line y1="48" y2="48" x1="40" x2="448.328125" class="ct-grid ct-vertical"></line><line y1="24" y2="24" x1="40" x2="448.328125" class="ct-grid ct-vertical"></line><line y1="0" y2="0" x1="40" x2="448.328125" class="ct-grid ct-vertical"></line></g><g><g class="ct-series ct-series-a"><line x1="57.013671875" x2="57.013671875" y1="120" y2="54.959999999999994" class="ct-bar" ct:value="542" opacity="1"></line><line x1="91.041015625" x2="91.041015625" y1="120" y2="66.84" class="ct-bar" ct:value="443" opacity="1"></line><line x1="125.068359375" x2="125.068359375" y1="120" y2="81.6" class="ct-bar" ct:value="320" opacity="1"></line><line x1="159.095703125" x2="159.095703125" y1="120" y2="26.400000000000006" class="ct-bar" ct:value="780" opacity="1"></line><line x1="193.123046875" x2="193.123046875" y1="120" y2="53.64" class="ct-bar" ct:value="553" opacity="1"></line><line x1="227.150390625" x2="227.150390625" y1="120" y2="65.64" class="ct-bar" ct:value="453" opacity="1"></line><line x1="261.177734375" x2="261.177734375" y1="120" y2="80.88" class="ct-bar" ct:value="326" opacity="1"></line><line x1="295.205078125" x2="295.205078125" y1="120" y2="67.92" class="ct-bar" ct:value="434" opacity="1"></line><line x1="329.232421875" x2="329.232421875" y1="120" y2="51.84" class="ct-bar" ct:value="568" opacity="1"></line><line x1="363.259765625" x2="363.259765625" y1="120" y2="46.8" class="ct-bar" ct:value="610" opacity="1"></line><line x1="397.287109375" x2="397.287109375" y1="120" y2="29.28" class="ct-bar" ct:value="756" opacity="1"></line><line x1="431.314453125" x2="431.314453125" y1="120" y2="12.599999999999994" class="ct-bar" ct:value="895" opacity="1"></line></g></g><g class="ct-labels"><foreignObject style="overflow: visible;" x="40" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">J</span></foreignObject><foreignObject style="overflow: visible;" x="74.02734375" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">F</span></foreignObject><foreignObject style="overflow: visible;" x="108.0546875" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">M</span></foreignObject><foreignObject style="overflow: visible;" x="142.08203125" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">A</span></foreignObject><foreignObject style="overflow: visible;" x="176.109375" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">M</span></foreignObject><foreignObject style="overflow: visible;" x="210.13671875" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">J</span></foreignObject><foreignObject style="overflow: visible;" x="244.1640625" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">J</span></foreignObject><foreignObject style="overflow: visible;" x="278.19140625" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">A</span></foreignObject><foreignObject style="overflow: visible;" x="312.21875" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">S</span></foreignObject><foreignObject style="overflow: visible;" x="346.24609375" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">O</span></foreignObject><foreignObject style="overflow: visible;" x="380.2734375" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">N</span></foreignObject><foreignObject style="overflow: visible;" x="414.30078125" y="125" width="34.02734375" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 34px; height: 20px;">D</span></foreignObject><foreignObject style="overflow: visible;" y="96" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">0</span></foreignObject><foreignObject style="overflow: visible;" y="72" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">200</span></foreignObject><foreignObject style="overflow: visible;" y="48" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">400</span></foreignObject><foreignObject style="overflow: visible;" y="24" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">600</span></foreignObject><foreignObject style="overflow: visible;" y="0" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">800</span></foreignObject><foreignObject style="overflow: visible;" y="-30" x="0" height="30" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 30px; width: 30px;">1000</span></foreignObject></g></svg></div>
									</div>
									<div class="card-body">
									  <h4 class="card-title">Email Subscriptions</h4>
									  <p class="card-category">Last Campaign Performance</p>
									</div>
									<div class="card-footer">
									  <div class="stats">
										<i class="material-icons">access_time</i> campaign sent 2 days ago
									  </div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="card card-chart">
									<div class="card-header card-header-danger">
									  <div class="ct-chart" id="completedTasksChart"><svg width="100%" height="30%" class="ct-chart-line" style="width: 100%; height: 30%;"><g class="ct-grids"><line x1="40" x2="40" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line x1="91.666015625" x2="91.666015625" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line x1="143.33203125" x2="143.33203125" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line x1="194.998046875" x2="194.998046875" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line x1="246.6640625" x2="246.6640625" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line x1="298.330078125" x2="298.330078125" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line x1="349.99609375" x2="349.99609375" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line x1="401.662109375" x2="401.662109375" y1="0" y2="120" class="ct-grid ct-horizontal"></line><line y1="120" y2="120" x1="40" x2="453.328125" class="ct-grid ct-vertical"></line><line y1="96" y2="96" x1="40" x2="453.328125" class="ct-grid ct-vertical"></line><line y1="72" y2="72" x1="40" x2="453.328125" class="ct-grid ct-vertical"></line><line y1="48" y2="48" x1="40" x2="453.328125" class="ct-grid ct-vertical"></line><line y1="24" y2="24" x1="40" x2="453.328125" class="ct-grid ct-vertical"></line><line y1="0" y2="0" x1="40" x2="453.328125" class="ct-grid ct-vertical"></line></g><g><g class="ct-series ct-series-a"><path d="M 40 92.4 C 91.666 30 91.666 30 91.666 30 C 143.332 66 143.332 66 143.332 66 C 194.998 84 194.998 84 194.998 84 C 246.664 86.4 246.664 86.4 246.664 86.4 C 298.33 91.2 298.33 91.2 298.33 91.2 C 349.996 96 349.996 96 349.996 96 C 401.662 97.2 401.662 97.2 401.662 97.2" class="ct-line"></path><line x1="40" y1="92.4" x2="40.01" y2="92.4" class="ct-point" ct:value="230" opacity="1"></line><line x1="91.666015625" y1="30" x2="91.676015625" y2="30" class="ct-point" ct:value="750" opacity="1"></line><line x1="143.33203125" y1="66" x2="143.34203125" y2="66" class="ct-point" ct:value="450" opacity="1"></line><line x1="194.998046875" y1="84" x2="195.008046875" y2="84" class="ct-point" ct:value="300" opacity="1"></line><line x1="246.6640625" y1="86.4" x2="246.6740625" y2="86.4" class="ct-point" ct:value="280" opacity="1"></line><line x1="298.330078125" y1="91.2" x2="298.340078125" y2="91.2" class="ct-point" ct:value="240" opacity="1"></line><line x1="349.99609375" y1="96" x2="350.00609375" y2="96" class="ct-point" ct:value="200" opacity="1"></line><line x1="401.662109375" y1="97.2" x2="401.672109375" y2="97.2" class="ct-point" ct:value="190" opacity="1"></line></g></g><g class="ct-labels"><foreignObject style="overflow: visible;" x="40" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">12p</span></foreignObject><foreignObject style="overflow: visible;" x="91.666015625" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">3p</span></foreignObject><foreignObject style="overflow: visible;" x="143.33203125" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">6p</span></foreignObject><foreignObject style="overflow: visible;" x="194.998046875" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">9p</span></foreignObject><foreignObject style="overflow: visible;" x="246.6640625" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">12p</span></foreignObject><foreignObject style="overflow: visible;" x="298.330078125" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">3a</span></foreignObject><foreignObject style="overflow: visible;" x="349.99609375" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">6a</span></foreignObject><foreignObject style="overflow: visible;" x="401.662109375" y="125" width="51.666015625" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 52px; height: 20px;">9a</span></foreignObject><foreignObject style="overflow: visible;" y="96" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">0</span></foreignObject><foreignObject style="overflow: visible;" y="72" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">200</span></foreignObject><foreignObject style="overflow: visible;" y="48" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">400</span></foreignObject><foreignObject style="overflow: visible;" y="24" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">600</span></foreignObject><foreignObject style="overflow: visible;" y="0" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 24px; width: 30px;">800</span></foreignObject><foreignObject style="overflow: visible;" y="-30" x="0" height="30" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 30px; width: 30px;">1000</span></foreignObject></g></svg></div>
									</div>
									<div class="card-body">
									  <h4 class="card-title">Completed Tasks</h4>
									  <p class="card-category">Last Campaign Performance</p>
									</div>
									<div class="card-footer">
									  <div class="stats">
										<i class="material-icons">access_time</i> campaign sent 2 days ago
									  </div>
									</div>
								</div>
							</div>
						</div>
						<!-- 3rd row -->
						<div class="row">
							<div class="col-lg-6 col-md-12">
								<div class="card">
									<div class="card-header card-header-tabs card-header-primary">
									  <div class="nav-tabs-navigation">
										<div class="nav-tabs-wrapper">
										  <span class="nav-tabs-title">Tasks:</span>
										  <ul class="nav nav-tabs" data-tabs="tabs">
											<li class="nav-item">
											  <a class="nav-link active" href="#profile" data-toggle="tab">
												<i class="material-icons">bug_report</i> Bugs
												<div class="ripple-container"></div>
											  </a>
											</li>
											<li class="nav-item">
											  <a class="nav-link" href="#messages" data-toggle="tab">
												<i class="material-icons">code</i> Website
												<div class="ripple-container"></div>
											  </a>
											</li>
											<li class="nav-item">
											  <a class="nav-link" href="#settings" data-toggle="tab">
												<i class="material-icons">cloud</i> Server
												<div class="ripple-container"></div>
											  </a>
											</li>
										  </ul>
										</div>
									  </div>
									</div>
									<div class="card-body">
									  <div class="tab-content">
										<div class="tab-pane active" id="profile">
										  <table class="table">
											<tbody>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="" checked="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Sign contract for "What are conference organizers afraid of?"</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
												</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="" checked="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Create 4 Invisible User Experiences you Never Knew About</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											</tbody>
										  </table>
										</div>
										<div class="tab-pane" id="messages">
										  <table class="table">
											<tbody>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="" checked="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
												</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Sign contract for "What are conference organizers afraid of?"</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											</tbody>
										  </table>
										</div>
										<div class="tab-pane" id="settings">
										  <table class="table">
											<tbody>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="" checked="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
												</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											  <tr>
												<td>
												  <div class="form-check">
													<label class="form-check-label">
													  <input class="form-check-input" type="checkbox" value="" checked="">
													  <span class="form-check-sign">
														<span class="check"></span>
													  </span>
													</label>
												  </div>
												</td>
												<td>Sign contract for "What are conference organizers afraid of?"</td>
												<td class="td-actions text-right">
												  <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit Task">
													<i class="material-icons">edit</i>
												  </button>
												  <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove">
													<i class="material-icons">close</i>
												  </button>
												</td>
											  </tr>
											</tbody>
										  </table>
										</div>
									  </div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-12">
								<div class="card">
									<div class="card-header card-header-warning">
									  <h4 class="card-title">Employees Stats</h4>
									  <p class="card-category">New employees on 15th September, 2016</p>
									</div>
									<div class="card-body table-responsive">
									  <table class="table table-hover">
										<thead class="text-warning">
										  <tr><th>ID</th>
										  <th>Name</th>
										  <th>Salary</th>
										  <th>Country</th>
										</tr></thead>
										<tbody>
										  <tr>
											<td>1</td>
											<td>Dakota Rice</td>
											<td>$36,738</td>
											<td>Niger</td>
										  </tr>
										  <tr>
											<td>2</td>
											<td>Minerva Hooper</td>
											<td>$23,789</td>
											<td>Curaçao</td>
										  </tr>
										  <tr>
											<td>3</td>
											<td>Sage Rodriguez</td>
											<td>$56,142</td>
											<td>Netherlands</td>
										  </tr>
										  <tr>
											<td>4</td>
											<td>Philip Chaney</td>
											<td>$38,735</td>
											<td>Korea, South</td>
										  </tr>
										</tbody>
									  </table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--   Core JS Files   -->
		<script src="assets/js/core/jquery.min.js"></script>
		<script src="assets/js/core/popper.min.js"></script>
		<script src="assets/js/core/bootstrap-material-design.min.js"></script>
		<script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
		<!-- Forms Validations Plugin -->
		<script src="assets/js/plugins/jquery.validate.min.js"></script>
		<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
		<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
		<!-- Chartist JS -->
		<script src="assets/js/plugins/chartist.min.js"></script>
		<!-- Control Center for Material Dashboard -->
		<script src="assets/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
		<script>
			$(document).ready(function() {
				md.initCharts();
				
				// Load data according to the project selected (default: All project)
				$("#projects-select").change(function(){
					this.form.submit();
				});
			});
		</script>
	</body>
</html>