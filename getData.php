<?php

	require_once ('lib/ActiveResource.php');
	
	class Issue extends ActiveResource {
		var $site = 'jean-noel.aubry:*Aubry34@srvrd:3000/redmine/';
		var $request_format = 'json';
		var $element_name = 'issue';
	}
	
	class Project extends ActiveResource {
		var $site = 'jean-noel.aubry:*Aubry34@srvrd:3000/redmine/';
		var $request_format = 'json';
		var $element_name = 'project';
	}
		

	// get last 24 hours date
	$last24hrs = date("Y-m-d", strtotime("-1 day"));
	
	// find issues
	$issue = new Issue();
	//$issues = $issue->find('all', $_GET);
	$params['updated_on'] = '>='.$last24hrs;
	$params['status_id'] = '*';
	$params['cf_8'] = '*';
	$params['limit'] = '1000';
	//$params['project_id'] = '2';
	$issues = sizeof($issue->find('all', $params));
	var_dump($issues);


?>
<!DOCTYPE html>
<html>
  <head>
    <title>My first Chartist Tests</title>
    <link href="assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet">
  </head>
  <body>
    <!-- Site content goes here !-->
	<div class="ct-chart ct-perfect-fourth"></div>
    <div id="weekNo"></div>
	
	<script src="assets/js/core/jquery.min.js"></script>
	<!-- Chartist JS -->
	<script src="assets/js/plugins/chartist.min.js"></script>
	<script>
		$(document).ready(function() {
			var data = {
			  // A labels array that can contain any sort of values
			  labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
			  // Our series array that contains series objects or in this case series data arrays
			  series: [
				[5, 2, 4, 2, 0]
			  ]
			};
			
			var options = {
			  width: 300,
			  height: 200
			};

			// Create a new line chart object where as first parameter we pass in a selector
			// that is resolving to our chart container element. The Second parameter
			// is the actual data object.
			new Chartist.Line('.ct-chart', data, options);
		});
		
		/* For a given date, get the ISO week number
		 *
		 * Based on information at:
		 *
		 *    http://www.merlyn.demon.co.uk/weekcalc.htm#WNR
		 *
		 * Algorithm is to find nearest thursday, it's year
		 * is the year of the week number. Then get weeks
		 * between that date and the first day of that year.
		 *
		 * Note that dates in one year can be weeks of previous
		 * or next year, overlap is up to 3 days.
		 *
		 * e.g. 2014/12/29 is Monday in week  1 of 2015
		 *      2012/1/1   is Sunday in week 52 of 2011
		 */
		function getWeekNumber(d) {
			// Copy date so don't modify original
			d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
			// Set to nearest Thursday: current date + 4 - current day number
			// Make Sunday's day number 7
			d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
			// Get first day of year
			var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
			// Calculate full weeks to nearest Thursday
			var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
			// Return array of year and week number
			return [weekNo];
		}
		
		var result = getWeekNumber(new Date(2019,1,7));
		$("#weekNo").html(result);

	</script>
  </body>
</html>